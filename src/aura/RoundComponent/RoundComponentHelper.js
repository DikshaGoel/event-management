({
	drawDefaultVenueLayout : function(component,event,helper) {
        
        debugger;
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        var numberOfSeats = component.get("v.wrapperObj.NumberOfSeats");
 		var tableName = component.get("v.wrapperObj.tableName");
        var tableId = component.get("v.tableId");
		var parentdiv = document.createElement('div');
        var obj = component.get("v.wrapperObj");
        parentdiv.setAttribute("style","position: relative; width: 120px; height: 120px; background-color: rgb(205, 133, 63);border-radius: 50% ");
        parentdiv.id = tableName+component.getGlobalId();
        parentdiv.className = 'testClass';
        parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
             var sObjectEvent = $A.get("e.c:EditLayoutEvent");
            sObjectEvent.setParams({
            "message": obj
            })
        sObjectEvent.fire();
        }));
        $("div[id='"+globalID+"']").append(parentdiv);
        var height = document.getElementById(tableName+globalID).offsetHeight;
        var width = document.getElementById(tableName+globalID).offsetWidth;
        var div = 360 / numberOfSeats;
        var radius = 90;
        if(numberOfSeats <= "10"){
            radius = 90;
        }else if(numberOfSeats <= "20"){
            radius = 135;
            parentdiv.setAttribute("style","position: relative; width: 180px; height: 180px; background-color: rgb(205, 133, 63);border-radius: 50% ");
        }else if(numberOfSeats <= "30"){
            parentdiv.setAttribute("style","position: relative; width: 270px; height: 270px; background-color: rgb(205, 133, 63);border-radius: 50% ");
           // document.getElementById(globalID).setAttribute("style","width:270px;height:270px");
            radius = 180; 
        }else if(numberOfSeats <= "40"){
            
            parentdiv.setAttribute("style","position: relative; width: 360px; height: 360px; background-color: rgb(205, 133, 63);border-radius: 50% ");
           // document.getElementById(globalID).setAttribute("style","width:270px;height:270px");
            radius = 180; 
        }
        var offsetToParentCenter = parseInt(parentdiv.offsetWidth / 2);  //assumes parent is square
        var offsetToChildCenter = 20;
        var totalOffset = offsetToParentCenter - offsetToChildCenter;
        for (var j = 1; j <= numberOfSeats; ++j){
            var childdiv = document.createElement('div');
            childdiv.className = 'div2';
            childdiv.setAttribute("style","position: absolute;width: 40px;height: 40px;background-color: #FFFFFF;border-radius: 100px;border: 1px dashed #060606;");
            childdiv.id = numberOfSeats;
           // childdiv.style.position = 'absolute';
            var y = Math.sin((div * j) * (Math.PI / 180)) * radius;
            var x = Math.cos((div * j) * (Math.PI / 180)) * radius;
            childdiv.style.top = (y + totalOffset).toString() + "px";
            childdiv.style.left = (x + totalOffset).toString() + "px";
            parentdiv.append(childdiv);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = tableName+'S'+j;
            childdiv.append(seatNumber);
        }   
    }
})
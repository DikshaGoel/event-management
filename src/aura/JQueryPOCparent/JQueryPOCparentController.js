({
	allowDrop: function(cmp, event, helper) {
        event.preventDefault();
    },
    drop: function(cmp, event, helper) {
        debugger;
        var imagesOnCanvas = [];
        var image = document.getElementById(event.dataTransfer.getData("image_id"));
        var iconDiv = document.createElement('div');
        $(iconDiv).attr("draggable", "true");
        //$(iconDiv).attr("ondragstart","{!c.drag}");
        iconDiv.id = "test"+image.id;
        //$(iconDiv).bind("drag", function() { helper.dragObject() });
        $(iconDiv).draggable({
            drag: function() {
                var offset = $(this).offset();
                var PositionX = offset.left;
                var PositionY = offset.top;
                console.log('X-' + PositionX + ' Y-' + PositionY);
            },

            stop: function() {
                var finalOffset = $(this).offset();
                var finalxPos = finalOffset.left;
                var finalyPos = finalOffset.top;
                $(this).draggable('option', 'revert', 'invalid');
                /*$(this).draggable('option', {revert: false, containment: 'body'});*/
                $('#finalX').text('Final X: ' + finalxPos);
                $('#finalY').text('Final X: ' + finalyPos);
            }
        });
        var parentElement = document.getElementById("span1");
        $('#span1').droppable({
            accept: '#imageId',
            over: function() {
                $(this).animate({
                    'border-width': '5px',
                    'border-color': '#0f0'
                }, 500);
                $('#imageId').draggable('option', 'containment', $(this));
            },

            stop: function() {
                var finalOffset = $(this).offset();
                var finalxPos = finalOffset.left;
                var finalyPos = finalOffset.top;

                $('#finalX').text('Final X: ' + finalxPos);
                $('#finalY').text('Final X: ' + finalyPos);
            },
            drop: function(event, ui) {
                ui.draggable.draggable('option', 'revert', true);
            }
        });

        var parentElement = document.getElementById("span1");
        parentElement.appendChild(iconDiv);
        event.preventDefault();

        


        var parentContent = $("#" + event.target.getAttribute("id")).parent();
        var testDiv = document.getElementById('test'+image.id);

        var $this = $(this);

        event.preventDefault();

        var width = $this.width();
        var height = $this.height();
        var cntrLeft = width / 2 - image.width / 2;
        var cntrTop = height / 2 - image.height / 2;

        var imageId = image.id;
        var divCss = document.getElementById(imageId);
        $(divCss).attr("ondragstart", "{!c.drag}");
        //divCss.style.left= cntrLeft + "px";
        divCss.style.left = cntrLeft + "px";
        divCss.style.top = cntrTop + "px";
        $("#" + image.id).clone().appendTo(testDiv);
        
        //$("#" + image.id).draggable();

    }
})
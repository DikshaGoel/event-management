({
        doInit: function(component, event, helper) {

            debugger;
            var globalID = component.getGlobalId;
            var action = component.get("c.getVenueLayoutType");
            var venueLayoutId = component.get("v.eventvenueId");
            console.log("eventID" + component.get("v.eventID"));
            action.setParams({ "venueLayoutId": venueLayoutId });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'ERROR') {
                    console.log(a.getError());
                } else if (state === 'SUCCESS') {
                    debugger;
                    component.set("v.wrapperList", response.getReturnValue());
                    console.log(component.get("v.wrapperList"));
                    var tableList = response.getReturnValue().tables;
                    console.log("tableList" + tableList);
                    var rowList = [];
                    var columnList = [];
                    var columns = response.getReturnValue().SeatsPerRow;
                    var rows = response.getReturnValue().numberOfTables / columns;
                    //if(rows < rows+.5 ){
                    //    rows++;
                    //}
                    var defaultlayoutType = response.getReturnValue().defaultlayoutType;
                    var totalSeats = response.getReturnValue().totalSeats;
                    var SeatsPerRow = response.getReturnValue().SeatsPerRow;
                    var Sections = response.getReturnValue().Sections;
                    for (var i = 0; i < rows; i++) {
                        rowList.push(i);
                    }
                    for (var j = 0; j < columns; j++) {
                        columnList.push(j);
                    }
                    component.set("v.numberOfRows", rowList);
                    component.set("v.numberOfColumns", columnList);
                    var rowListData = [];
                    var counter = 1;
                    for (var row = 0; row < rows; row++) {
                        for (var column = 0; column < columns; column++) {
                            if (counter <= tableList.length) {
                                rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter - 1], "defaultlayoutType": defaultlayoutType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections })
                                counter++;
                            }
                        }
                    }
                    console.log(rowListData);
                    component.set("v.tempList", rowListData);
                    helper.doInitHelper(component, event, helper);
                     (function() {
                        console.log('Loading....');
                        var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
                        window.requestAnimationFrame = requestAnimationFrame;
                    })();
                    //helper.renderScene();
                    //requestAnimationFrame(helper.renderScene);
                   
                }
            });
            $A.enqueueAction(action);
            
        },
        saveLayout: function(component) {

            debugger;
            var tablesList = component.get("v.wrapperList.tables");
            var updatedWrapperList = component.get("v.tempList");
            var action = component.get("c.saveVenueLayout");
            for (var i = 0, len = updatedWrapperList.length; i < len; i++) {
                delete updatedWrapperList[i].rowNo;
                delete updatedWrapperList[i].colNo;
            }
            component.set("v.wrapperList.tables", updatedWrapperList);
            console.log("updatedWrapperList" + updatedWrapperList);
            action.setParams({ "wrapper": component.get("v.wrapperList") });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'ERROR') {

                } else if (state === 'SUCCESS') {
                    debugger;
                }
            });
            $A.enqueueAction(action);
        },

        onLayoutChange: function(component, event) {
            console.log('on layout select :' + event.getSource().get('v.value')); //only works for aura:component like <ui:inputSelect/>

            var action = component.get("c.getVenueLayoutTypes");
            var layout = event.getSource().get('v.value'); //component.get("v.recordId");
            action.setParams({ layout: layout });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log("Success");
                    component.set("v.layoutTypeList", result);
                }
            });
            $A.enqueueAction(action);
        },

        onTypeChange: function(component, event) {
            console.log('on type select :' + event.getSource().get('v.value')); //only works for aura:component like <ui:inputSelect/>
        },

        backToVenueDetails: function(component, event, helper) {
            debugger;
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef: "c:venuelayout",
                componentAttributes: {
                    eventvenueId: component.get("v.eventvenueId")
                }
            });
            evt.fire();
            event.preventDefault();
        },
        handleApplicationEvent: function(component, event, helper) {

            debugger;
            var message = event.getParam("message");
            alert('message NumberOfSeats' + message.NumberOfSeats);
            alert('message tableName' + message.tableName);
            alert('message layoutType' + message.layoutType);
            var tableList = component.get("v.wrapperList.tables");
            for (var i = 0; i < tableList.length; i++) {
                if (message.tableName === tableList[i].tableName) {
                    tableList[i].NumberOfSeats = 9;
                }
            }
            component.set("v.wrapperList.tables", tableList);
            component.set("v.seatsPerTableAfterEdit", message.NumberOfSeats);
            component.set("v.tableNameAfterEdit", message.tableName);
            component.set("v.layoutTypeAfterEdit", message.layoutType);
        },
        /*drop : function(event) {
            debugger;
            var listitem = event.dataTransfer.getData("divid");
            
            console.log("Inside drop =====> "+listitem);    
            alert("element dropped here    "+listitem);
        },
    
        allowDrop: function(event){
            event.preventDefault();
        }*/
        /*onDragOver: function(component, event) {
            debugger;
            event.preventDefault();
        },

        drop: function(component, event, helper) {
            debugger;
            event.stopPropagation();
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';
            var files = event.dataTransfer.files;
            if (files.length > 1) {
                return alert("You can only upload one profile picture");
            }

        }*/
        allowDrop: function(cmp, event, helper) {
            event.preventDefault();
        },
        /*drop: function(cmp, event, helper) {
            debugger;
            var imagesOnCanvas = [];
            event.preventDefault();
            
            var image = document.getElementById(event.dataTransfer.getData("image_id"));
          
         
            var parentContent = $("#" + event.target.getAttribute("id")).parent();
            var listitem = event.dataTransfer.getData("text/plain");
          
            $("#" + image.id).clone().appendTo(event.target)
            event.preventDefault();
        },*/
        drop: function(cmp, event, helper) {

            debugger;
            var imageOnStackCanvas = [];
            var imagesOnCanvas = [];
            var eachIcon = cmp.get("v.iconList");
            event.preventDefault();
            var image = document.getElementById(event.dataTransfer.getData("image_id"));
            
            var mouse_position_x = event.dataTransfer.getData("mouse_position_x");
            var mouse_position_y = event.dataTransfer.getData("mouse_position_y");

            var canvas = document.getElementById('canvas');
            var ctx = canvas.getContext('2d');
            var width = image.clientWidth;
            var height = image.clientHeight;
            var clientXPosition = event.clientX;
            var strX = clientXPosition + '';
            var clientYPosition = event.clientY;
            var strY = clientYPosition + '';
            

            imagesOnCanvas.push({
                context: ctx,
                image: image,
                /*x: event.clientX - canvas.offsetLeft - mouse_position_x + event.clientX,
                y: event.clientY - canvas.offsetTop - mouse_position_y + event.clientY,*/
                x: event.clientX - canvas.offsetLeft,
                y: event.clientY - canvas.offsetTop,
                width: image.clientWidth,
                height: image.clientHeight

            });


            
            for (var x = 0, len = imagesOnCanvas.length; x < len; x++) {
                var obj = imagesOnCanvas[x];
                obj.context.drawImage(obj.image, obj.x, obj.y, width, height);
                eachIcon.push(imagesOnCanvas[x]);
                cmp.set("v.iconList", eachIcon);
                imageOnStackCanvas.push(eachIcon);

                var drag = false;
                var dragStart;
                var dragEnd;


                canvas.addEventListener('mousedown', function(event) {
                    dragStart = {
                      x: event.pageX - canvas.offsetLeft,
                      y: event.pageY - canvas.offsetTop,
                      width: image.clientWidth,
                      height: image.clientHeight
                    }

                    drag = true;

                  });

                  canvas.addEventListener('mousemove', function(event) {
                    if (drag) {
                      dragEnd = {
                        x: event.pageX - canvas.offsetLeft,
                        y: event.pageY - canvas.offsetTop,
                        width: image.clientWidth,
                        height: image.clientHeight
                      }
                      ctx.translate(dragEnd.x - dragStart.x, dragEnd.y - dragStart.y);
                      //clear();
                      //draw();
                        ctx.save();
                        for (var x = 0, len = imagesOnCanvas.length; x < len; x++) {
                            var obj = imagesOnCanvas[x];
                            ctx.clearRect(0, 0, canvas.width, canvas.height);
                        }
                        ctx.setTransform(1, 0, 0, 1, 0, 0);
                        //ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.restore();

                       for (var x = 0, len = eachIcon.length; x < len; x++) {
                            var obj = eachIcon[x];
                            obj.context.drawImage(obj.image, obj.x, obj.y, width, height);
                            ctx.save();
                        }
                      dragStart = dragEnd
                    }

                  });
                  
                  canvas.addEventListener('mouseup',function(event){
                    drag = false;
                  });
            }

          //  var obj = imagesOnCanvas[x];
           
            event.preventDefault();
            /*var parentContent = $("#" + event.target.getAttribute("id")).parent();
            var listitem = event.dataTransfer.getData("text/plain");*/
            //moving of icons
            /* A S K I
            
             canvas.onmousedown = function(event) {
                var downX = event.offsetX,
                    downY = event.offsetY;

                for (var x = 0, len = imagesOnCanvas.length; x < len; x++) {
                    var obj = imagesOnCanvas[x];
                    
                    if (!helper.isPointInRange(downX, downY, obj)) {
                        continue;
                    }
                    obj.context.drawImage(obj.image, obj.x, obj.y, width, height);
                    helper.startMove(obj, downX, downY);
                    break;
                }
                 //helper.renderScene(event,helper);

            }*/
            debugger;
            var action = cmp.get("c.getEventVenueIdforIcon");
            var eventvenueId = cmp.get("v.eventvenueId"); //cmp.get("v.recordId");
            action.setParams({ "idForIcon": eventvenueId, "imageName": image.id, "xAxisPosition": strX, "yAxisPosition": strY });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log("Success");
                    //cmp.set("v.layoutTypeList", result);
                }
            });
            $A.enqueueAction(action);

        //For saving icons in list client Side
        
         /*   var ARLMisclist = cmp.get("v.designMiscIconList"); 
            var ARLObj = {};
            ARLObj= cmp.get("v.designMiscIconObj");
            var PositionObj = cmp.get("v.designPosIconObj");
             
            ARLObj.objectType = image.id; 
            ARLObj.guid = generateGUID();
            PositionObj.positionX=strX;
            PositionObj.positionY=strY;
            ARLObj.position = PositionObj;
            ARLMisclist.push(ARLObj);
            console.log();*/

        },
        save: function(cmp, event, helper) {
            /*var action = cmp.get("c.getEventVenueIdforIcon");
            var eventvenueId = cmp.get("v.eventvenueId"); //cmp.get("v.recordId");
            action.setParams({ "idForIcon": eventvenueId, "imageName": image.id, "xAxisPosition": strX, "yAxisPosition": strY });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log("Success");
                    //cmp.set("v.layoutTypeList", result);
                }
            });
            $A.enqueueAction(action);*/
        },



})
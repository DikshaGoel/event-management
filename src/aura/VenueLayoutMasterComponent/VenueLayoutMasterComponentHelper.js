({
    doInitHelper: function(component, event, helper) {
        var eventvenueId = component.get("v.eventvenueId");

        console.log('Init venueLayoutController');
        //component.set("v.flag",true);

        var action = component.get("c.getEventVenues");
        action.setParams({ "eventvenueId": eventvenueId });

        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var venue = response.getReturnValue();
            if (state === "SUCCESS") {

                component.set("v.venue", venue);

            }



        });
        $A.enqueueAction(action);

    },
    startMove: function(obj, downX, downY) {
        debugger;
        var imagesOnCanvas = [];
        var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        var origX = obj.x,
            origY = obj.y;
        canvas.onmousemove = function(event) {
            var moveX = event.offsetX,
                moveY = event.offsetY;
            var diffX = moveX - downX,
                diffY = moveY - downY;

            var width = obj.clientWidth;
            var height = obj.clientHeight;

            obj.x = origX + diffX;
            obj.y = origY + diffY;

        }

        canvas.onmouseup = function() {
            var context = canvas.getContext('2d');

            var width = obj.clientWidth;
            var height = obj.clientHeight;



            // stop moving
            canvas.onmousemove = function() {};
        }
    },
    isPointInRange: function(x, y, obj) {
        return !(x < obj.x ||
            x > obj.x + obj.width ||
            y < obj.y ||
            y > obj.y + obj.height);
    },

    /*renderScene: function(event) {
        debugger;
        console.log('renderScene IN....');
        var b = false;
        if (b == true) {
            requestAnimationFrame(this.renderScene);
        }
        b = true;

         var imagesOnCanvas = [];
        var image = document.querySelectorAll('#images img');
        var dt = event.dataTransfer;
        dt.setData("image_id");
        var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        var width = image.clientWidth;
        var height = image.clientHeight;


        for (var x = 0, len = imagesOnCanvas.length; x < len; x++) {
            var obj = imagesOnCanvas[x];
            obj.context.drawImage(obj.image, obj.x, obj.y, width, height);
        }
    }
*/
})
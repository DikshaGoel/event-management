({
    doInitdata :function(component, event,helper) {
        debugger;
        helper.pickListValues(component,event);
        var action = component.get("c.getcampign");
        var recordId = component.get("v.recordId");
        action.setParams({recordId:recordId});
        action.setCallback(this, function(response) {
            var res = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log("Success");
                component.set("v.junc",res);
                 helper.onSelect(component,event);
                 helper.totalDurationOfEvent(component,event);
                           }
        });
        $A.enqueueAction(action);
    },

    saveEventbutton : function(component,event,helper){
        debugger;
        var helpResult = helper.validateFields(component,event);
        if(helpResult==false){
        var action = component.get("c.saveEvent");
        var emps=component.get("v.junc")
        action.setParams({ "emp" :emps });
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var stringItems = response.getReturnValue();
            
         if (state === "SUCCESS"){
              var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Success!",
                           "title": "Success!",
                          "message": "Record updated Successfully"
                               });
                     toastEvent.fire();
                   
                 
            }
       
        });
        $A.enqueueAction(action)
        }
  },
    
    saveEventNext : function(component,event,helper){
        debugger;
       var helpResult = helper.validateFields(component,event);
        if(helpResult==false){
         var action = component.get("c.saveEvent");
        var emps=component.get("v.junc")
        action.setParams({ "emp" :emps });
        action.setCallback(this, function(response) {
            debugger;
            var stringItems = response.getReturnValue();
            var state = response.getState();
              if (state === "SUCCESS"){
              var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Success!",
                           "title": "Success!",
                          "message": "Record updated Successfully",
                               });
                     toastEvent.fire();
                   component.set('v.defaultTabId',"venueid");
                 
            }
     });
        $A.enqueueAction(action);
    }
            
},
    selectAllDay : function(component, event,helper){
        debugger;
        var endDate = component.find("endDate").get("v.value");
        var allDay = component.find("allDay").get("v.value");
        var startDate = component.find("startDate").get("v.value");
        
        if(allDay == true){
            component.set("v.junc.End_Date__c", startDate);
             helper.totalDurationOfEvent(component,event);
        }
    },
    
    onSelectCon : function(component, event,helper){
        debugger;
        helper.totalDurationOfEvent(component,event);
    }
  
    
    
})
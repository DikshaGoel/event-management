({
    validateFields: function(component, event) {
        debugger;
        var flag = false;
        var startDate = component.find("startDate").get("v.value");
        var endDate = component.find("endDate").get("v.value");
        var eventName = component.find("Name").get("v.value");
        var active = component.find("activeCheckbox").get("v.value");
        var s = new Date(startDate);
        var e = new Date(endDate);
        var date = new Date();
        if( s < date  && active == true){
            flag = true;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" : "Error!",
                "title": "Error!",
                "message": " Start Date and Time should not be less than today's Date and Time ",
            });
            toastEvent.fire();
            
            event.preventDefault();
        }
        if( s > e && active == true){
            flag = true;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" : "Error!",
                "title": "Error!",
                "message": " End Date and Time should not be less than Start Date and Time",
            });
            toastEvent.fire();
            
            event.preventDefault();
            
        }
        
        if( /[^a-zA-z0-9\-\/]/.test( eventName ) ) {
            flag = true;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" : "Error!",
                "title": "Error!",
                "message": "Special Charecters Are Not Allowed",
            });
            toastEvent.fire();
            
            event.preventDefault();
            
            
        }
        return flag;
    },
    
    pickListValues : function(component,event){
        debugger;
        var action = component.get("c.getPickListValue");
        var inputsel = component.find("InputSelectDynamic");
        action.setCallback(this, function(response) {
            var res = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                var opts=[];
                for(var i=0;i< res.length;i++){
                    opts.push({"class": "optionClass", label: res[i], value: res[i]});
                }
                inputsel.set("v.options", opts);
            }
        });
        $A.enqueueAction(action)
    },
      onSelect: function(component, event) {
            debugger;
             var startDate = component.find("startDate").get("v.value");
             var endDate = component.find("endDate").get("v.value");
             var allDay = component.find("allDay").get("v.value");
       
          if(allDay == true){
            component.set("v.junc.End_Date__c", startDate);
          }
       
        },
            
      totalDurationOfEvent : function(component,event){
          debugger;
           var startDate = component.find("startDate").get("v.value");
             var endDate = component.find("endDate").get("v.value");
            var allDay = component.find("allDay").get("v.value")

           if(startDate != undefined && startDate != null && startDate != '' && endDate != undefined && endDate != null && endDate != '')
        {
            var s = new Date(startDate);
			var e = new Date(endDate);
			var timeDiff = Math.abs(e.getTime() - s.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var day, hour, minute, seconds;
            seconds = Math.floor(timeDiff / 1000);
            minute = Math.floor(seconds / 60);
            seconds = seconds % 60;
            hour = Math.floor(minute / 60);
        	component.set("v.junc.Event_Duration__c",diffDays);//*24
                      
            
        }
      
          
         if((diffDays == 0 && hour < 24)||(diffDays == 1 && hour < 24)){ 
              component.set("v.junc.All_Day_Event__c", true);
              
         }else{
            component.set("v.junc.All_Day_Event__c", false);
         }
      }
})
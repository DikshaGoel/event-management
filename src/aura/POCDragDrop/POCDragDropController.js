/*
interact('.dropzone').dropzone({
  accept: '#yes-drop',
  overlap: 0.75,

  ondropactivate: function (event) {
    event.target.classList.add('drop-active');
  },
  ondragenter: function (event) {
    var draggableElement = event.relatedTarget,
        dropzoneElement = event.target;
    dropzoneElement.classList.add('drop-target');
    draggableElement.classList.add('can-drop');
    draggableElement.textContent = 'Dragged in';
  },
  ondragleave: function (event) {
    event.target.classList.remove('drop-target');
    event.relatedTarget.classList.remove('can-drop');
    event.relatedTarget.textContent = 'Dragged out';
  },
  ondrop: function (event) {
    event.relatedTarget.textContent = 'Dropped';
  },
  ondropdeactivate: function (event) {
    event.target.classList.remove('drop-active');
    event.target.classList.remove('drop-target');
  }
});*/
({
    drag: function(cmp, event, helper) {
        //store the position 
        event.dataTransfer.setData("mouse_position_x", event.clientX - event.target.offsetLeft);
        event.dataTransfer.setData("mouse_position_y", event.clientY - event.target.offsetTop);

        event.dataTransfer.setData("image_id", event.target.id);
    },
    allowDrop: function(cmp, event, helper) {
        event.preventDefault();
    },
    drop: function(cmp, event, helper) {
        debugger;
        var imagesOnCanvas = [];
        event.preventDefault();
        var image = document.getElementById(event.dataTransfer.getData("image_id"));

        var mouse_position_x = event.dataTransfer.getData("mouse_position_x");
        var mouse_position_y = event.dataTransfer.getData("mouse_position_y");

        var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');	
        var width = image.clientWidth;
        var height = image.clientHeight;
        var clientXPosition = event.clientX;
        var strX = clientXPosition + '';
        var clientYPosition = event.clientY;
        var strY = clientYPosition + '';
        imagesOnCanvas.push({
            context: ctx,  
            image: image,
            x:event.clientX - canvas.offsetLeft - mouse_position_x,
            y:event.clientY - canvas.offsetTop - mouse_position_y,
           
            width: image.clientWidth,
            height: image.clientHeight

        });

        for (var x = 0, len = imagesOnCanvas.length; x < len; x++) {
            var obj = imagesOnCanvas[x];

        }

        /*var parentContent = $("#" + event.target.getAttribute("id")).parent();
        var listitem = event.dataTransfer.getData("text/plain");*/
        obj.context.drawImage(obj.image,obj.x,obj.y,width,height);

        event.preventDefault();

    }
});
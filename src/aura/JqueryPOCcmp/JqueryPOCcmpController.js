({
    drag: function(cmp, event, helper) {
        //store the position 
        event.dataTransfer.setData("mouse_position_x", event.clientX - event.target.offsetLeft);
        event.dataTransfer.setData("mouse_position_y", event.clientY - event.target.offsetTop);

        event.dataTransfer.setData("image_id", event.target.id);
        var image = document.getElementById(event.dataTransfer.getData("image_id"));
        var imageId = image.id;
        $(function() {
            $(imageId).draggable({
                drag: function() {
                    var offset = $(this).offset();
                    var PositionX = offset.left;
                    var PositionY = offset.top;
                    console.log('X-' + PositionX + ' Y-' + PositionY);
                }
            });
        });

    },
    allowDrop: function(cmp, event, helper) {
        event.preventDefault();
    },
    drop: function(cmp, event, helper) {
        debugger;
        var imagesOnCanvas = [];
        var image = document.getElementById(event.dataTransfer.getData("image_id"));
        var iconDiv = document.createElement('div');
        $(iconDiv).attr("draggable", "true");
        //$(iconDiv).attr("ondragstart","{!c.drag}");
        iconDiv.id = "test" + image.id;
        $(iconDiv).attr("width",image.width +'px');
        $(iconDiv).attr("height",image.height +'px');

        //$(iconDiv).bind("drag", function() { helper.dragObject() });
        $(iconDiv).draggable({
            drag: function() {
                var offset = $(this).offset();
                var PositionX = offset.left;
                var PositionY = offset.top;
                console.log('X-' + PositionX + ' Y-' + PositionY);
            },

            stop: function() {
                var finalOffset = $(this).offset();
                var finalxPos = finalOffset.left;
                var finalyPos = finalOffset.top;
                /*$(this).draggable('option', 'revert', 'invalid');*/
                /*$(this).draggable('option', {revert: false, containment: parentElement});*/
                $('#finalX').text('Final X: ' + finalxPos);
                $('#finalY').text('Final X: ' + finalyPos);
            }
        });
        var parentElement = document.getElementById("span1");
        $('#span1').droppable({
            accept: '#imageId',
            over: function() {
                $(this).animate({
                    'border-width': '5px',
                    'border-color': '#0f0'
                }, 500);
                /*$('#imageId').draggable('option', 'containment', $(this));*/
            },

            stop: function() {
                var finalOffset = $(this).offset();
                var finalxPos = finalOffset.left;
                var finalyPos = finalOffset.top;

                $('#finalX').text('Final X: ' + finalxPos);
                $('#finalY').text('Final X: ' + finalyPos);
            },
            drop: function(event, ui) {
                ui.draggable.draggable('option', 'revert', false);
            }
        });

        var parentElement = document.getElementById("span1");
        parentElement.appendChild(iconDiv);
        event.preventDefault();
        var parentContent = $("#" + event.target.getAttribute("id")).parent();
        var testDiv = document.getElementById('test' + image.id);

        var $this = $(this);

        event.preventDefault();

        var width = $this.width();
        var height = $this.height();
		var strX = width + '';
        var strY = height + '';
        var cntrLeft = width / 2 - image.width / 2;
        var cntrTop = height / 2 - image.height / 2;
		var eventIDhard = 'a027F000001nCz9QAE';

        var imageId = image.id;
        var divCss = document.getElementById(imageId);
        $(divCss).attr("ondragstart", "{!c.drag}");
        //divCss.style.left= cntrLeft + "px";
        /*       divCss.style.left = cntrLeft + "px";
               divCss.style.top = cntrTop + "px";*/
        $("#" + image.id).clone().appendTo(testDiv);

        //$("#" + image.id).draggable();
        var action = cmp.get("c.getEventVenueIdforIcon");
            var eventvenueId = cmp.get("v.eventvenueId"); //cmp.get("v.recordId");
            action.setParams({ "idForIcon": eventIDhard, "imageName": image.id, "xAxisPosition": strX, "yAxisPosition": strY });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log("Success");
                    //cmp.set("v.layoutTypeList", result);
                }
            });
            $A.enqueueAction(action);
    }

})
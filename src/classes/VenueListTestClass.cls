@isTest
public class VenueListTestClass {
    @isTest
    public static void allVenue(){
        List<Venue__c> venList = new List<Venue__c>();
        List<Venue__c> newList = new  List<Venue__c>();
        for(Integer i=0;  i < 10; i++){
            Venue__c ven = new Venue__c();
            if(i/2 == 0){
                ven.name = 'oldEvent'+i;
            }
            else{
                ven.name = 'newEvent'+i;
            }
        venList.add(ven);
        }
        insert venList;
        test.startTest();
        newList= venuelistapex.aLLVenue();
        test.stopTest();
        System.assertEquals(10, newList.size());
        
    }
    @isTest
    public static void searchVenue(){
         List<Venue__c> venList = new List<Venue__c>();
        List<Venue__c> newList = new  List<Venue__c>();
        for(Integer i=0;  i < 10; i++){
            Venue__c ven = new Venue__c();
            if(i/2==0){
                ven.name = 'oldEvent'+i;
            }
            else{
                ven.name = 'newEvent'+i;
            }
        venList.add(ven);
        }
        insert venList;
        test.StartTest();
        newList= venuelistapex.findVenueByName('oldEvent');
        test.stopTest();
        System.assertEquals(2, newList.size());
        
    }
	@isTest
    public static void getSubvenue(){
         List<Venue__c> venList = new List<Venue__c>();
        for(Integer i=0;  i < 10; i++){
            Venue__c ven = new Venue__c();
            if(i/2==0){
                ven.name = 'oldEvent'+i;
            }
            else{
                ven.name = 'newEvent'+i;
            }
        venList.add(ven);
        }
        insert venList;
		
	    List<Venue__c> newList = new List<Venue__c> ();
		Venue__c venueObj = [SELECT Id,Name
							FROM Venue__c
							WHERE Name = 'oldEvent0'
							LIMIT 1];
		list<Venue__c> updateList = new list<Venue__c>();
		  for( Venue__c ven : [SELECT Id,Name,ParentVenue__c
				FROM Venue__c
				WHERE Name != 'oldEvent0']){
		  
		   ven.ParentVenue__c = venueObj.Id;
		   updateList.add(ven);
		  }
		  update updateList;
		  List<String> objId = new List<String>();
          objId.add(venueObj.Id);
		  test.startTest();
		  newList = venuelistapex.getsubVenueList(objId);
		  test.stoptest();
		  System.assertEquals(9,newList.size()); 
			}
				
        
    }
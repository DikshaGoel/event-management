public class VenueLayoutArrangementController {
   @AuraEnabled
    public static DesignLayoutWrapper getVenueLayoutType(String venueLayoutId){
        
        
      //  System.debug('venueLayoutId' + venueLayoutId);
    //	Event_Venue__c evtVenue = [Select Id,
      //                           		  Venue_Layout__c from Event_Venue__c where Id =: venueLayoutId LIMIT 1];
        
        Event_Venue__c evtVenue = [Select Id,
                              		  Venue_Layout__c from Event_Venue__c where Id =: venueLayoutId LIMIT 1];
        
        Venue_Layout__c venueLayout = new Venue_Layout__c();
        venueLayout = [SELECT Id,
                       		  Type__c,
                       		  Layout__c,
                              No_of_Seats__c,
                              No_of_Tables__c,
                              Layout_Row_Count__c,
                              Horizontal_Seats__c,
                              LayoutDetails__c,
                              Seats_Per_Row__c
                              from Venue_Layout__c WHERE Id =: evtVenue.Venue_Layout__c ];//'a017F000000diKoQAI' ] ;
        DesignLayoutWrapper wrapperObject = new DesignLayoutWrapper();
        wrapperObject = (DesignLayoutWrapper) JSON.deserializeStrict(venueLayout.LayoutDetails__c, DesignLayoutWrapper.class);
        System.debug('wrapperObject' + wrapperObject);
        return wrapperObject;
    }
    @AuraEnabled
    public static void saveVenueLayout(DesignLayoutWrapper wrapper){
    
        System.debug('wrapper' + wrapper);
    }
    
    /* A S K I I
		: Retriving header details for which requires Event Venue.. 
	*/
    @AuraEnabled
    public static Event_Venue__c getEventHeaderDetails(String eventVenueId){
		if (eventVenueId == null) {
            return null;
        } 
		 Event_Venue__c eventVenue = [
            SELECT Id
                 , Campaign__c
            	 , Campaign__r.Name
            	 , Venue__c
                 , Venue__r.Name
                 , Venue_Layout__c
                 , Venue_Layout__r.Name
              FROM Event_Venue__c
            where Id =: eventVenueId
        ];   	
        return eventVenue != null ? eventVenue : new Event_Venue__c();
    }
    
    @AuraEnabled
    public static Venue__c getEventVenues(String eventvenueId){
        if (eventVenueId == null) {
            return null;
        } 
		 Event_Venue__c eventVenue = [
            SELECT Id
                 , Campaign__c
            	 , Campaign__r.Name
            	 , Venue__c
                 , Venue__r.Name
                 , Venue_Layout__c
                 , Venue_Layout__r.Name
              FROM Event_Venue__c
            where Id =: eventVenueId
        ];  
        if(eventVenue!= null){
      Venue__c venue = [
          SELECT Id
          ,Name
         ,Street_Line_1__c
          ,Street_Line_2__c
          ,City__c
          ,Country__c
          ,Zipcode__c
          ,Location__c
          ,Seating_Capacity__c
          ,Active__c
          ,Type__c 
          FROM Venue__c 
          where id = :eventVenue.Venue__c];
        system.debug('venue' +venue);
        return venue;
        }else{
             return null;
        }
       
    }
    
    @AuraEnabled
  public static String generateGUID() {
        return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(DateTime.now().getTime().format())));
    }
	
    @AuraEnabled
    public static Event_Venue__c getEventVenueIdforIcon(String idForIcon ,String imageName, String xAxisPosition ,String yAxisPosition) {
      System.debug('newwwwwwwwwwwwwwwIDDDDDDD'+idForIcon);
      System.debug('image Nameeeee'+imageName);
      System.debug('image xAxisPosition'+xAxisPosition);
      System.debug('image yAxisPosition'+yAxisPosition);
  Event_Venue__c eventVenue = [SELECT Id
                 , Campaign__c
              , Campaign__r.Name
              , Venue__c
                 , Venue__r.Name
                 , Venue_Layout__c
                 , Venue_Layout__r.Name
              FROM Event_Venue__c
            where Id =: idForIcon
        ];
        Venue_Layout__c newvenueLayout = new Venue_Layout__c();
        newvenueLayout = [SELECT Id,
                            Type__c,
                            Layout__c,
                              No_of_Seats__c,
                              No_of_Tables__c,
                              Layout_Row_Count__c,
                              Horizontal_Seats__c,
                              LayoutDetails__c,
                              Seats_Per_Row__c
                              from Venue_Layout__c WHERE Id =: eventVenue.Venue_Layout__c ];//'a017F000000diKoQAI' ] ;
        


        System.debug('newvenueLayout newvenueLayout' + newvenueLayout);

        String layoutIconDetailsJson = '';                      
        DesignLayoutWrapper newwrapperObject = new DesignLayoutWrapper();
        newwrapperObject = (DesignLayoutWrapper) JSON.deserializeStrict(newvenueLayout.LayoutDetails__c, DesignLayoutWrapper.class);
        System.debug('wrapperObject' + newwrapperObject);
        if(newwrapperObject.miscellaneous != null){


        List<DesignLayoutWrapper.Miscellaneous> oldMiscObjList = new List<DesignLayoutWrapper.Miscellaneous>();
        oldMiscObjList = newwrapperObject.miscellaneous;
        DesignLayoutWrapper.Miscellaneous misc = new DesignLayoutWrapper.Miscellaneous ();
        DesignLayoutWrapper.Position pos = new DesignLayoutWrapper.Position ();
         misc.objectType = imageName; 
            misc.guid = generateGUID();
            pos.positionX=xAxisPosition;
            pos.positionY=yAxisPosition;
            misc.position = pos;
            oldMiscObjList.add(misc);
           
          layoutIconDetailsJson =  JSON.serialize(newwrapperObject);
          System.debug('JSON ::: '+layoutIconDetailsJson);

          //Venue_Layout__c venueLayout = new Venue_Layout__c();
        }else{

          List<DesignLayoutWrapper.Miscellaneous> newIconsList = new List<DesignLayoutWrapper.Miscellaneous>();
          DesignLayoutWrapper.Miscellaneous misc = new DesignLayoutWrapper.Miscellaneous ();
          DesignLayoutWrapper.Position pos = new DesignLayoutWrapper.Position ();
          misc.objectType = imageName; 
          misc.guid = generateGUID();
          pos.positionX=xAxisPosition;
          pos.positionY=yAxisPosition;
          misc.position = pos;
          newIconsList.add(misc);
          newwrapperObject.miscellaneous = newIconsList;
          layoutIconDetailsJson =  JSON.serialize(newwrapperObject);
          System.debug('JSON ::: '+layoutIconDetailsJson);
        }
         try {
            newvenueLayout.LayoutDetails__c = layoutIconDetailsJson;
            update newvenueLayout;
                       
        } catch(Exception ex) {
            System.debug('Exception :: ' + ex.getMessage());
        }
         return null;

  }
}